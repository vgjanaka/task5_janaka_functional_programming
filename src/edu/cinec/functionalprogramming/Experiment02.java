package edu.cinec.functionalprogramming;

public class Experiment02 {

    public static void main(String[] args) {

        Doctor[] doctorArray = {new Doctor("Dr.Saman", "Perera", 37, 8, 54)
                , (new Doctor("Dr.Kamal", "Gunawrdhana", 48, 6, 45)),
                new Doctor("Dr.Ruwan", "Samera", 35, 5, 58),
                new Doctor("Dr.Pawan", "Wirasekara", 30, 5, 44),
                new Doctor("Dr.Malith", "Gunasekara", 37, 7, 48)};

        for (Doctor doctor : doctorArray) {
            System.out.println(doctor);
        }
    }
}
