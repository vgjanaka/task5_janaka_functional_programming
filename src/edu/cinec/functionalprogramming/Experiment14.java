package edu.cinec.functionalprogramming;

import java.util.stream.Stream;

public class Experiment14 {

    class DoctorStreamBuilder {
        public Stream<Doctor> build(){
            Stream.Builder<Doctor> builder = Stream.builder();
            builder.add(new Doctor("Dr.Saman", "Perera", 37, 8, 54));
            builder.add(new Doctor("Dr.Kamal", "Gunawrdhana", 48, 6, 45));
            builder.add(new Doctor("Dr.Ruwan", "Samera", 35, 5, 58));
            builder.add( new Doctor("Dr.Pawan", "Wirasekara", 30, 5, 44));
            return builder.build();
        }
    }

    public void run() {
        DoctorStreamBuilder builder = new DoctorStreamBuilder();
        Stream <Doctor> nameStream = builder.build();
        nameStream.forEach(doctor -> System.out.println(doctor));
    }

    public static void main(String[] args) {
        new Experiment14().run();
    }
}
