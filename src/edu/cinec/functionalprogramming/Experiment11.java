package edu.cinec.functionalprogramming;

import java.util.Arrays;
import java.util.List;

public class Experiment11 {
    public void run() {
        Doctor[] doctorArray = {new Doctor("Dr.Saman", "Perera", 37, 8, 54)
                , (new Doctor("Dr.Kamal", "Gunawrdhana", 48, 6, 45)),
                new Doctor("Dr.Ruwan", "Samera", 35, 5, 58),
                new Doctor("Dr.Pawan", "Wirasekara", 30, 5, 44),
                new Doctor("Dr.Malith", "Gunasekara", 37, 7, 48)};

        List<Doctor> doctorList = Arrays.asList(doctorArray);

        doctorList.stream().map(doctor -> doctor.getFastName().replaceAll("Dr.","MBBS Doctor "))
                .forEach(doctor -> System.out.println(doctor));

    }

    public static void main(String[] args) {
        new Experiment11().run();
    }
}
