package edu.cinec.functionalprogramming;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Experiment07 {

    public void run() {
        Doctor[] doctorArray = {new Doctor("Dr.Saman", "Perera", 37, 8, 54)
                , (new Doctor("Dr.Kamal", "Gunawrdhana", 48, 6, 45)),
                new Doctor("Dr.Ruwan", "Samera", 35, 5, 58),
                new Doctor("Dr.Pawan", "Wirasekara", 30, 5, 44),
                new Doctor("Dr.Malith", "Gunasekara", 37, 7, 48)};

        List<Doctor> doctorList = Arrays.asList(doctorArray);

        doctorList.forEach(new Consumer<Doctor>() {
            public void accept(Doctor doctor) {
                System.out.println(doctor);
            }
        });
    }

    public static void main(String[] args) {
        new Experiment07().run();
    }
}
