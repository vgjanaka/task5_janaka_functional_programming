package edu.cinec.functionalprogramming;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class Experiment13 {

    class DoctorSupplier implements Supplier<Doctor> {
        Doctor[] doctorArray = {new Doctor("Dr.Saman", "Perera", 37, 8, 54)
                , (new Doctor("Dr.Kamal", "Gunawrdhana", 48, 6, 45)),
                new Doctor("Dr.Ruwan", "Samera", 35, 5, 58),
                new Doctor("Dr.Pawan", "Wirasekara", 30, 5, 44),
                new Doctor("Dr.Malith", "Gunasekara", 37, 7, 48)};

        List<Doctor> doctorList = Arrays.asList(doctorArray);
        int nextIndex = 0;
        public Doctor get() {
            if(nextIndex< doctorList.size()) {
                return doctorList.get(nextIndex++);
            }
            return null;
        }
    }

    public void run() {
        DoctorSupplier ds = new DoctorSupplier();
        System.out.println(ds.get());
        System.out.println(ds.get());
        System.out.println(ds.get());
        System.out.println(ds.get());
    }

    public static void main(String[] args) {
        new Experiment13().run();
    }
}
